﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace Papafon
{
    public class NumberInfo : INotifyPropertyChanged
    {
        private int dbid;
        private string name;
        private string farbe;
        private string nummer;
        private string subject;
        private string textfarbe;
        private ImageSource image;
        public int orderIndex;

        public int DbId
        {
            get { return dbid; }
            set
            {
                DbId = value;
                OnPropertyChanged("DbId");
            }
        }


        public string Name
        {
            get { return name; }
            set
            {
                Name = value;
                OnPropertyChanged("Name");
            }
        }

        public string Nummer
        {
            get { return nummer; }
            set
            {
                nummer = value;
                OnPropertyChanged("Nummer");
            }
        }

        public string Farbe
        {
            get { return farbe; }
            set
            {
                nummer = value;
                OnPropertyChanged("Farbe");
            }
        }

        public string TextTextfarbe
        {
            get { return textfarbe; }
            set
            {
                nummer = value;
                OnPropertyChanged("Textfarbe");
            }
        }

        public int OrderIndex
        {
            get { return orderIndex; }
            set
            {
                orderIndex = value;
                OnPropertyChanged("OrderIndex");
            }
        }

        public string Subject
        {
            get
            {
                return subject;
            }

            set
            {
                subject = value;
                OnPropertyChanged("Subject");
            }
        }

        public ImageSource InboxImage
        {
            get { return this.image; }
            set
            {
                this.image = value;
                OnPropertyChanged("InboxImage");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
        }


    }
}
