﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Papafon
{
    public class ListViewSwipingViewModel
    {
        private ObservableCollection<NumberInfo> inboxInfo;
        public ListViewSwipingViewModel()
        {
            GenerateSource();
        }

        public ObservableCollection<NumberInfo> InboxInfo
        {
            get { return inboxInfo; }
            set { this.inboxInfo = value; }
        }

        private void GenerateSource()
        {
            NumberInfoRepository inboxinfo = new NumberInfoRepository();
            inboxInfo = inboxinfo.GetInboxInfo();
        }
    }

   

    


}
