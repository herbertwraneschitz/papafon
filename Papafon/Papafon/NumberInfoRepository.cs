﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Papafon
{
    public class NumberInfoRepository
    {
        private ObservableCollection<NumberInfo> numberInfo;

        public ObservableCollection<NumberInfo> NumberInfo
        {
            get { return numberInfo; }
            set { this.numberInfo = value; }
        }

        internal ObservableCollection<NumberInfo> GetInboxInfo()
        {
            var empInfo = new ObservableCollection<NumberInfo>();
            for (int i = 0; i < NumberInfo.Count(); i++)
            {
                var record = new NumberInfo()
                {
                   
                  
                    InboxImage = ImageSource.FromResource("Swiping.Images.InboxIcon.png"),
                   
                };
                
                empInfo.Add(record);
            }
            return empInfo;
        }

        

        private void Delete()
        {
            return;
        }

    }
}
