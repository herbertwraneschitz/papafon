﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Papafon.Interfaces
{
    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}
