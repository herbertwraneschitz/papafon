﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Papafon.Interfaces
{
    public interface IImageService
    {

        Task<byte[]> ResizeToExactSize(byte[] source, byte[] sourcePattern);

        Task<byte[]> ResizeImage(byte[] source, float maxWidth, float maxHeight);



    }
}
