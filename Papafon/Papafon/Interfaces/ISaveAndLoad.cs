﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Papafon.Interfaces
{
    public interface ISaveAndLoad
    {
        Task<string> SaveImageAsync(string filename, byte[] image);
        Task<byte[]> LoadImageAsync(string filename);

        bool FileExists(string path);

        string GetFullPath(string filename);
    }
}
