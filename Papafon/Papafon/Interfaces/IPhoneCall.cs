﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Papafon.Interfaces
{
    public interface IPhoneCall
    {
        void MakeQuickCall(string PhoneNumber);
    }
}
