﻿using Papafon.DBItems;
using Papafon.Interfaces;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.IO;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Papafon.Common
{
    public class CameraHelper
    {
        //Für aus Galerie
        public async Task<MediaFile> GetPhoto()
        {
            MediaFile file = null;
            if (CrossMedia.Current.IsCameraAvailable && CrossMedia.Current.IsTakePhotoSupported)
            {

                file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                {
                    DefaultCamera = CameraDevice.Rear
                });

                return file;
            }
            return null;
        }

        public async Task<MediaFile> PickPhoto()
        {

            MediaFile file = null;
            if (CrossMedia.Current.IsPickPhotoSupported)
            {
                file = await CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
                {
                    PhotoSize = PhotoSize.Medium,

                });

                return file;
                

                //Foto.Source = ImageSource.FromFile(_photo.Path);

                //MemoryStream ms = new MemoryStream();

                //Stream input = _photo.GetStream();
                //input.CopyTo(ms);
                //var bildData = ms.ToArray();
            }
            return null;
        }

        public async Task<DBItems.ImageItem> GetImage()
        {
            var statusCam = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
            var statusPhoto = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);
            var statusContacts = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Contacts);
            var statusPhone = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Phone);
            var statusSpeech = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Speech);
            var statusMicro = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Microphone);

            if (statusCam != PermissionStatus.Granted)
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera });
                statusCam = results[Permission.Camera];
            }

            if (statusPhoto != PermissionStatus.Granted)
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Storage });
                statusPhoto = results[Permission.Storage];
            }
            if (statusContacts != PermissionStatus.Granted)
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Phone });
                statusPhone = results[Permission.Phone];
            }
            if (statusContacts != PermissionStatus.Granted)
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Speech });
                statusSpeech = results[Permission.Speech];
            }
            if (statusContacts != PermissionStatus.Granted)
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Microphone });
                statusMicro = results[Permission.Microphone];
            }
            if (statusContacts != PermissionStatus.Granted)
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Contacts });
                statusContacts = results[Permission.Contacts];
            }

            if (statusPhoto == PermissionStatus.Granted && statusCam == PermissionStatus.Granted && statusPhone == PermissionStatus.Granted &&
                statusSpeech == PermissionStatus.Granted && statusMicro == PermissionStatus.Granted && statusContacts == PermissionStatus.Granted)
            {
                await CrossMedia.Current.Initialize();

                MediaFile photo = await GetPhoto();

                if (photo != null)
                {
                    ImageItem dbImage = new ImageItem();

                    dbImage.Date = DateTime.Now;

                    //dbImage.ImageFilePath = photo.AlbumPath;

                    var image = await DependencyService.Get<IImageService>().ResizeImage(ReadFully(photo.GetStream()), 2600, 2600);
                    var thumbnail = await DependencyService.Get<IImageService>().ResizeImage(image, 180, 180);

                    string imageBaseName = string.Format("{0}.jpg", Guid.NewGuid());

                    dbImage.ImageFileName = imageBaseName;

                    dbImage.ImageFilePath = await DependencyService.Get<ISaveAndLoad>().SaveImageAsync(imageBaseName, image);
                    dbImage.ImageSmallFilePath = await DependencyService.Get<ISaveAndLoad>().SaveImageAsync(string.Format("{0}_{1}", "thumb", imageBaseName), thumbnail);

                    return dbImage;
                }
            }
            else
            {
                await App.Current.MainPage.DisplayAlert("Nicht Berechtigt", "Berechtigung notwendig", "Ok");

                
            }
            return null;
        }

        public async Task<DBItems.ImageItem> GetImageFromGallery()
        {
            var statusCam = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
            var statusPhoto = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

            if (statusCam != PermissionStatus.Granted)
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera });
                statusCam = results[Permission.Camera];
            }

            if (statusPhoto != PermissionStatus.Granted)
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Storage });
                statusPhoto = results[Permission.Storage];
            }

            if (statusPhoto == PermissionStatus.Granted && statusCam == PermissionStatus.Granted)
            {
                await CrossMedia.Current.Initialize();

                MediaFile photo = await PickPhoto();

                if (photo != null)
                {
                    ImageItem dbImage = new ImageItem();

                    dbImage.Date = DateTime.Now;

                    //dbImage.ImageFilePath = photo.AlbumPath;

                    var image = await DependencyService.Get<IImageService>().ResizeImage(ReadFully(photo.GetStream()), 2600, 2600);
                    var thumbnail = await DependencyService.Get<IImageService>().ResizeImage(image, 180, 180);

                    string imageBaseName = string.Format("{0}.jpg", Guid.NewGuid());

                    dbImage.ImageFileName = imageBaseName;

                    dbImage.ImageFilePath = await DependencyService.Get<ISaveAndLoad>().SaveImageAsync(imageBaseName, image);
                    dbImage.ImageSmallFilePath = await DependencyService.Get<ISaveAndLoad>().SaveImageAsync(string.Format("{0}_{1}", "thumb", imageBaseName), thumbnail);

                    return dbImage;
                }
            }
            else
            {
                await App.Current.MainPage.DisplayAlert("Nicht Berechtigt", "Berechtigung notwendig", "Ok");


            }
            return null;
        }



        public static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {

                
                input.CopyTo(ms);
                Task.Delay(1000);
                return ms.ToArray();
            }
        }
    }
}
