﻿using Papafon.Interfaces;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xamarin.Forms;

namespace Papafon.DBItems
{
    public class ImageItem
    {
        [PrimaryKey, AutoIncrement]
        
        public int DbId { get; set; }

        public string ImageStorePath { get; set; }

        public string ImageFileName { get; set; }

        public string ImageFilePath { get; set; }

        public byte[] ImageResult { get; set; }

        public string ResultImageFilePath { get; set; }



        public string ResultImageThumbnailFileName { get; set; }

        private string imageSmallFilePath;

        public string ImageSmallFilePath
        {
            get
            {
                return DependencyService.Get<ISaveAndLoad>().GetFullPath(imageSmallFilePath);
            }

            set
            {
                imageSmallFilePath = value;
            }
        }





        public ImageSource ImageResultSource
        {
            get
            {
                if (ImageResult != null)
                {
                    return ImageSource.FromStream(() => new MemoryStream(ImageResult));
                }
                else
                    return null;
            }
        }
        public DateTime Date { get; set; }
    }

    
    
}
