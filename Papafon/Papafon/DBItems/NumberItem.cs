﻿using SQLite;
using Xamarin.Forms;

namespace Papafon.DBItems
{
    public class NumberItem
    {
       

        [PrimaryKey, AutoIncrement]
        public int DbId { get; set; }

        public string Name { get; set; }

        public string Nummer { get; set; }

        public string Farbe { get; set; }

        public string Textfarbe { get; set; }

        public int OrderIndex { get; set; }

        public byte[] DokBytes { get; set; }

        public string Bildpfad { get; set; }

        public bool IstBildSichtbar { get; set; }

        public bool IstInitialSichtbar { get; set; }

       

    }
}
