﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Papafon.DBItems
{
    public class EinstellungItem
    {
        [PrimaryKey, AutoIncrement]
        public int DbId { get; set; }

        public bool IstTonAn { get; set; }

        public bool IstClearSichtbar { get; set; }

        

    }
}
