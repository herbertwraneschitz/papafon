﻿using Papafon.DBItems;
using Papafon.Interfaces;
using SQLite;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Linq;

namespace Papafon
{

    public class AppDatabase
    {
        static object locker = new object();
        SQLiteConnection database;
        public AppDatabase()
        {
            database = DependencyService.Get<ISQLite>().GetConnection();
            database.CreateTable<NumberItem>();
            database.CreateTable<EinstellungItem>();
            database.CreateTable<ImageItem>();

        }

        public ImageItem GetImageItem(int id)
        {
            lock (locker)
            {
                return database.Table<ImageItem>().Where(i => i.DbId == id).FirstOrDefault();
            }
        }

        public ImageItem GetImageData()
        {
            lock (locker)
            {
                return database.Table<ImageItem>().FirstOrDefault();
            }
        }


        //public ImageItem GetImagItemByRequestId(string requestId)
        //{
        //    lock (locker)
        //    {
        //        return database.Table<ImageItem>().Where(i => i.RequestId == requestId).FirstOrDefault();
        //    }
        //}

        public int SaveImageData(ImageItem item)
        {
            lock (locker)
            {
                if (item.DbId != 0)
                {
                    database.Update(item);
                    return item.DbId;
                }
                else
                {
                    return database.Insert(item);
                }
            }
        }

        public int SaveNumberItem(NumberItem item)
        {
            lock (locker)
            {
                if (item.DbId != 0)
                {
                    database.Update(item);
                    return item.DbId;
                }
                else
                {
                    return database.Insert(item);
                }
            }
        }


        public int OvverideSaveNumber(NumberItem item)
        {
            lock (locker)
            {
                if (item.DbId != 0)
                {
                    database.Update(item);
                    return item.DbId;
                }
                else
                {
                    return database.Update(item);
                }
            }
        }

        public int OvverideSaveNumberId(NumberItem item)
        {
            lock (locker)
            {
                if (item.DbId != 0)
                {
                    database.Update(item);
                    return item.DbId;
                }
                else
                {
                    return database.Update(item);
                }
            }
        }

        public int SaveEinstellungItem(EinstellungItem item)
        {
            lock (locker)
            {
                if (item.DbId != 0)
                {
                    database.Update(item);
                    return item.DbId;
                }
                else
                {
                    return database.Insert(item);
                }
            }
        }

        public EinstellungItem GetEinstellungItem()
        {
            lock (locker)
            {
                return database.Table<EinstellungItem>().FirstOrDefault();
            }
        }

        public IEnumerable<ImageItem> GetImageItems()
        {
            lock (locker)
            {
                                                 
                    return (from i in database.Table<ImageItem>() select i).OrderBy(i => i.Date).Reverse().ToList();
            }
        }

        public NumberItem GetNumberItem()
        {
            lock (locker)
            {
                return database.Table<NumberItem>().FirstOrDefault();
            }
        }

        public void DeleteNumbers()
        {
            database.DeleteAll<NumberItem>();
        }

        public void DeleteNumber(NumberItem item)
        {
            database.Delete<NumberItem>(item.DbId);
        }

        public List<NumberItem> GetNumberItems()
        {
            lock (locker)
            {
                return database.Table<NumberItem>().ToList();
            }
            
        }

        public NumberItem GetNumberById(int id)
        {
            lock (locker)
            {
                return database.Table<NumberItem>().Where(i => i.DbId == id).FirstOrDefault();
            }
        }



    }
}
