﻿using Papafon.DBItems;
using Papafon.Interfaces;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Plugin.TextToSpeech;
using System;
using System.IO;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Diagnostics;
using Papafon.Common;

namespace Papafon.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AnlegenPage : ContentPage
    {
        //ImageSource _imageSource;
        //private IMedia _mediaPicker;
        //Image image;

        ImageItem _imageItem;
        ImageItem _imageItemGallery;
        AbsoluteLayout layout = new AbsoluteLayout();

        int _id;
        public AnlegenPage(int? id = null)
        {
            NavigationPage.SetHasNavigationBar(this, true);
            InitializeComponent();
            _id = id.HasValue ? id.Value : 0;
        }

        bool _ton;

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

        }

        private async void Foto_Clicked(object sender, System.EventArgs e)
        {

            CameraHelper cameraHelper = new CameraHelper();

            var action = await DisplayActionSheet("Quelle wählen", "Abrechen", null, "Gallerie", "Neues Foto");

            if (action == "Gallerie")
            {
                ImageItem image = await cameraHelper.GetImageFromGallery();

                _imageItemGallery = image;

                if (image != null)
                {
                    App.Database.SaveImageData(image);
                    im_Foto.Source = image.ImageSmallFilePath;

                }
            }

            else if (action == "Neues Foto")
            {
                ImageItem image = await cameraHelper.GetImage();

                _imageItem = image;

                if (image != null)
                {
                    App.Database.SaveImageData(image);
                    im_Foto.Source = image.ImageSmallFilePath;

                }
            }

            else if (action == "Abrechen")
                return;

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (_imageItemGallery == null && _imageItem == null)
            {
                im_Foto.Source = "man.png";
            }

            if (App.Database.GetEinstellungItem() != null)
            {
                var ton = App.Database.GetEinstellungItem();
                _ton = ton.IstTonAn;
            }

            if (_id != 0)
            {
                var currentItem = App.Database.GetNumberById(_id);

                enName.Placeholder = "Neuer Name";
                enNummer.Placeholder = "Neue Nummer";

                lb_Name.Text = currentItem.Name;
                lb_Nummer.Text = currentItem.Nummer;

                //if (enNummer.Text != null)
                //{
                //    currentItem.Nummer = enNummer.Text;
                //}
                //if (enName.Text != null)
                //{
                //    currentItem.Name = enName.Text;
                //}

                if (currentItem.Bildpfad == null)
                {
                    im_Foto.Source = "man.png";
                }
                else
                {
                    im_Foto.Source = currentItem.Bildpfad;
                }

                if (currentItem.Farbe == "#ff0000")
                {
                    notfall.IsToggled = true;
                }
            }
        }

        private async void save_Clicked(object sender, ClickedEventArgs e)
        {
            if (_id != 0)
            {
                var currentItem = App.Database.GetNumberById(_id);

                

                if (enNummer.Text != null)
                {
                    currentItem.Nummer = enNummer.Text;
                }
                else
                {
                    currentItem.Nummer = lb_Nummer.Text;
                }
                if (enName.Text != null)
                {
                    currentItem.Name = enName.Text;
                }
                else
                {
                    currentItem.Name = lb_Name.Text;
                }



                if (_imageItem != null || _imageItemGallery != null)
                {
                    if (_imageItem != null)
                    {

                        currentItem.Bildpfad = _imageItem.ImageSmallFilePath;
                        App.Database.SaveNumberItem(currentItem);

                    }
                    if (_imageItemGallery != null)
                    {

                        currentItem.Bildpfad = _imageItemGallery.ImageSmallFilePath;
                        App.Database.SaveNumberItem(currentItem);

                    }
                }



                if (notfall.IsToggled)
                {
                    currentItem.Farbe = "#ff0000";
                    currentItem.Textfarbe = "#ffffff";
                    if (_ton)
                    {
                        await CrossTextToSpeech.Current.Speak(currentItem.Name + AppResources.Notfallknopf);
                    }
                }
                else
                {
                    currentItem.Farbe = "#fafafa";
                    currentItem.Textfarbe = "#000000";
                }

                //if (enName.Text == null || enNummer.Text == null)
                //{
                //    if (_ton)
                //    {
                //        await CrossTextToSpeech.Current.Speak(AppResources.LeerEintrag);
                //    }

                //    return;
                //}
                App.Database.OvverideSaveNumber(currentItem);

                if (_ton)
                {
                    await CrossTextToSpeech.Current.Speak(currentItem.Name + AppResources.Geaendert);
                }
            }
            else
            {
                NumberItem item = new NumberItem();
                item.Name = enName.Text;
                item.Nummer = enNummer.Text;

                if (_imageItem != null)
                {
                    item.Bildpfad = _imageItem.ImageSmallFilePath;
                }
                else if (_imageItemGallery != null)
                {
                    item.Bildpfad = _imageItemGallery.ImageSmallFilePath;
                }

                if (notfall.IsToggled)
                {

                    item.Farbe = "#ff0000";
                    item.Textfarbe = "#ffffff";
                    if (_ton)
                    {
                        await CrossTextToSpeech.Current.Speak(item.Name + AppResources.Notfallknopf);
                    }
                }
                else
                {
                    item.Farbe = "#fafafa";
                    item.Textfarbe = "#000000";
                }

                if (enName.Text == null || enNummer.Text == null)
                {
                    if (_ton)
                    {
                        await CrossTextToSpeech.Current.Speak(AppResources.LeerEintrag);
                    }
                    else
                    {
                        await DisplayAlert("Nummer und Name eingeben", "", "OK");
                    }

                    return;
                }
                App.Database.SaveNumberItem(item);

                if (_ton)
                {
                    await CrossTextToSpeech.Current.Speak(item.Name + AppResources.InListe);
                }
            }


            //animationView.IsPlaying = true;

            //do
            //{
            //    animationView.IsVisible = true;
            //} while (animationView.IsPlaying = true);

            ////while (animationView.IsPlaying == true)
            ////{
            ////    animationView.IsVisible = true;
            ////}

            App.Current.MainPage = new MainPage();
        }

        private void abbrechen_Clicked(object sender, ClickedEventArgs e)
        {
            App.Current.MainPage = new MainPage();
        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }


    }
}



