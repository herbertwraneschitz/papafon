﻿using Papafon.Interfaces;
using Plugin.TextToSpeech;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Papafon.Views;
using Papafon.DBItems;
using System.Collections.ObjectModel;
using System.Linq;
using Syncfusion.ListView.XForms;
using System.Collections.Generic;
using System.Threading.Tasks;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;

namespace Papafon
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        private ObservableCollection<NumberItem> obervableListDataSource;


        string wahl = AppResources.Waehle;
        bool _clear;
        bool _ton;
        NumberItem _imgaeItem;

        public MainPage()
        {
            NavigationPage.SetHasNavigationBar(this, true);
            InitializeComponent();


            obervableListDataSource = new ObservableCollection<NumberItem>();

            lv.ItemDragging += ListView_ItemDraggingAsync;

            lv.ItemsSource = obervableListDataSource;

            //lvFoto.ItemDragging += ListView_ItemDraggingAsync;

            //lvFoto.ItemsSource = obervableListDataSource;


        }

        private void UpdateObervableListDataSource()
        {
            //lv.ItemsSource = null;
            var numberItems = App.Database.GetNumberItems();

            foreach (var item in numberItems)
            {
                if (obervableListDataSource.Where(i => i.DbId == item.DbId).Count() == 0)
                {
                    obervableListDataSource.Add(item);

                }

            }

            foreach (var item in obervableListDataSource.ToList())
            {
                if (numberItems.Where(i => i.DbId == item.DbId).Count() == 0)
                    obervableListDataSource.Remove(item);
            }
        }

        private void UpdateUiOnMain()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                UpdateObervableListDataSource();

            });
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            //Granted
            var statusContacts = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Contacts);
            var statusPhone = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Phone);
            var statusSpeech = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Speech);
            var statusMicro = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Microphone);

            if (statusPhone != PermissionStatus.Granted)
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Phone });
                statusPhone = results[Permission.Phone];
            }
            if (statusSpeech != PermissionStatus.Granted)
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Speech });
                statusSpeech = results[Permission.Speech];
            }
            if (statusMicro != PermissionStatus.Granted)
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Microphone });
                statusMicro = results[Permission.Microphone];
            }
            if (statusContacts != PermissionStatus.Granted)
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Contacts });
                statusContacts = results[Permission.Contacts];
            }

            //Test
            var dbitem = App.Database.GetNumberItems();

            foreach (var item in dbitem)
            {
                if (item.Bildpfad != null)
                {
                    item.IstBildSichtbar = true;
                    //item.IstInitialSichtbar = false;
                }
                else
                {
                    item.IstBildSichtbar = false;
                    //item.IstInitialSichtbar = true;
                }
                App.Database.SaveNumberItem(item);
            }



            UpdateObervableListDataSource();


            if (App.Database.GetEinstellungItem() == null)
            {
                EinstellungItem ton = new EinstellungItem();
                ton.IstTonAn = true;
                ton.IstClearSichtbar = true;
                App.Database.SaveEinstellungItem(ton);
                _ton = ton.IstTonAn;
                _clear = ton.IstClearSichtbar;
            }
            else
            {
                var holeTon = App.Database.GetEinstellungItem();
                _ton = holeTon.IstTonAn;
                _clear = holeTon.IstClearSichtbar;
                if (_ton)
                {

                    speaker.Source = "speaker.png";
                    holeTon.IstClearSichtbar = true;
                }
                else
                {
                    speaker.Source = "mute.png";
                    //holeTon.IstClearSichtbar = false;
                }
                App.Database.SaveEinstellungItem(holeTon);
            }

            MessagingCenter.Subscribe<App>(this, "UpdateTon", (sender) =>
            {

                //CrossTextToSpeech.Current.Dispose();
                //clear.IsEnabled = true;



            });


            if (App.Database.GetNumberItems() != null && App.Database.GetNumberItems().Count != 0)
            {
                if (_ton == true)
                {
                    clear.IsVisible = true;
                }
                else
                {
                    clear.IsVisible = false;
                }

                brille.IsVisible = false;

                //var indexer = App.Database.GetNumberItems();


                lv.ItemsSource = obervableListDataSource;


                if (_ton)
                {
                    await CrossTextToSpeech.Current.Speak(AppResources.Anruf);
                }

            }
            else
            {
                brille.IsVisible = true;
                //clear.IsVisible = false;
                lv.ItemsSource = null;

                if (_ton)
                {
                    await CrossTextToSpeech.Current.Speak(AppResources.Zuerst);
                }
            }
        }

        private void neu_Clicked(object sender, ClickedEventArgs e)
        {
            neu.IsEnabled = false;

            App.Current.MainPage = new AnlegenPage();
            neu.IsEnabled = true;

        }

        private async void name_Tapped(object sender, TappedEventArgs e)
        {
            lv.IsEnabled = false;
            var item = ((Xamarin.Forms.Grid)sender).BindingContext as NumberItem;
            var text = wahl + item.Name;

            if (_ton)
            {
                if (item.Farbe == "#ea4335")
                {
                    await CrossTextToSpeech.Current.Speak(AppResources.Notruf + item.Name);
                }
                else
                {
                    await CrossTextToSpeech.Current.Speak(text);
                }
            }
            DependencyService.Get<IPhoneCall>().MakeQuickCall(item.Nummer);
            lv.IsEnabled = true;


        }

        NumberItem _item;

        private async void leosch_Clicked(object sender, ClickedEventArgs e)
        {


            clear.IsEnabled = false;
            if (_ton)
            {


                await CrossTextToSpeech.Current.Speak(AppResources.LongClick);

            }
            else
            {
                lv.IsVisible = false;
                cv_info.IsVisible = true;

            }

            clear.IsEnabled = true;
        }

        private void Speaker_Tapped(object sender, EventArgs e)
        {

            if (App.Database.GetEinstellungItem() != null)
            {
                var ton = App.Database.GetEinstellungItem();

                if (ton.IstTonAn)
                {
                    ton.IstTonAn = false;
                    speaker.Source = "mute.png";
                    //clear.IsVisible = false;

                }
                else
                {
                    ton.IstTonAn = true;
                    speaker.Source = "speaker.png";
                    if (App.Database.GetNumberItems().Count != 0)
                    {
                        clear.IsVisible = true;
                    }
                }
                App.Database.SaveEinstellungItem(ton);
                _ton = ton.IstTonAn;
                _clear = clear.IsVisible;

                MessagingCenter.Send<App>((App)Xamarin.Forms.Application.Current, "UpdateTon");
            }

        }

        Image leftImage;
        Image rightImage;
        int itemIndex = -1;

        private void ListView_SwipeStarted(object sender, SwipeStartedEventArgs e)
        {
            itemIndex = -1;
        }

        private void ListView_Swiping(object sender, SwipingEventArgs e)
        {
            if (e.ItemIndex == 1 && e.SwipeOffSet > 70)
                e.Handled = true;
        }

        private void ListView_SwipeEnded(object sender, SwipeEndedEventArgs e)
        {
            itemIndex = e.ItemIndex;
        }

        private void rightImage_BindingContextChanged(object sender, EventArgs e)
        {
            var item = ((Image)sender).BindingContext as NumberItem;
            _imgaeItem = item;
            if (rightImage == null)
            {
                rightImage = sender as Image;
                (rightImage.Parent as View).GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(Delete) });
                rightImage.Source = "Delete.png";
            }
        }

        private void leftImage_BindingContextChanged(object sender, EventArgs e)
        {
            var item = ((Image)sender).BindingContext as NumberItem;
            _imgaeItem = item;
            if (leftImage == null)
            {
                leftImage = sender as Image;
                (leftImage.Parent as View).GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(Aendern) });
                leftImage.Source = "settings.png";
            }
        }

        private void Aendern()
        {
            App.Current.MainPage = new AnlegenPage(_imgaeItem.DbId);

            this.lv.ResetSwipe();
        }

        private async void Delete()
        {
            //animation

            this.lv.ResetSwipe();

            if (_ton)
            {
                await CrossTextToSpeech.Current.Speak(_imgaeItem.Name + AppResources.Gel);
            }

            App.Database.DeleteNumber(_imgaeItem);

            UpdateObervableListDataSource();

            //EmptyLabel

            var count = App.Database.GetNumberItems().Count;
            if (count == 0)
            {
                lv.ItemsSource = null;
                brille.IsVisible = true;
                //clear.IsVisible = false;
                if (_ton)
                {
                    await CrossTextToSpeech.Current.Speak(AppResources.ListLeer);
                }
            }
            else
            {
                brille.IsVisible = false;
                lv.IsVisible = true;

                lv.ItemsSource = obervableListDataSource;

            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

        }




        private void lv_SelectionChanged(object sender, ItemSelectionChangedEventArgs e)
        {



        }

        private void lv_SelectionChanging(object sender, ItemSelectionChangingEventArgs e)
        {


        }

        private void back_Clicked(object sender, EventArgs e)
        {
            lv.IsVisible = true;
            cv_info.IsVisible = false;

        }

        private async void ListView_ItemDraggingAsync(object sender, ItemDraggingEventArgs e)
        {
            //var item = ((Syncfusion.ListView.XForms.SfListView)sender).BindingContext as NumberItem;

            if (e.Action == DragAction.Drop)
            {
                if (e.OldIndex > e.NewIndex)
                {
                    await Task.Delay(100);


                    ChangeData(e);
                }

            }
        }



        private void ChangeData(ItemDraggingEventArgs e)
        {

            obervableListDataSource.Move(e.OldIndex, e.NewIndex);



            lv.ItemsSource = obervableListDataSource;

            foreach (var item in obervableListDataSource)
            {

                item.OrderIndex = e.NewIndex;
                App.Database.SaveNumberItem(item);
            }


        }

        private void Foto_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new FotoAnsichPage();
        }
    }
}
