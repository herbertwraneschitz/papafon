﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Papafon.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FotoAnsichPage : ContentPage
	{
		public FotoAnsichPage ()
		{
			InitializeComponent ();
		}

        private void Home_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new MainPage();
        }
    }
}