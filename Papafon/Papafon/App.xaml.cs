﻿using Xamarin.Forms;
using Papafon.Interfaces;
using Xamarin.Forms.Xaml;
using Papafon.DBItems;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace Papafon
{
    public partial class App : Application
	{
        private static AppDatabase _database;

        public static byte[] CroppedImage;

        public App ()
		{
			InitializeComponent();

            MainPage = new ContentPage();

            if (Device.OS == TargetPlatform.iOS || Device.OS == TargetPlatform.Android)
            {
                var ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();

                AppResources.Culture = ci; // set the RESX for resource localization

                DependencyService.Get<ILocalize>().SetLocale(ci); // set the Thread for locale-aware methods
            }

        }

        public static string Translate(string key)
        {
            return AppResources.ResourceManager.GetString(key, AppResources.Culture);
        }

        protected override void OnStart ()
		{
            // Handle when your app starts
            App.Current.MainPage = new MainPage();

            
           


        }

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}

        public static AppDatabase Database
        {
            get
            {
                if (_database == null)
                {
                    _database = new AppDatabase();
                }
                return _database;
            }
        }

    }
}
