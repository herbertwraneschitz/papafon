﻿using Papafon.Droid;
using Papafon.Interfaces;
using System.IO;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLite_Android))]
namespace Papafon.Droid
{
    public class SQLite_Android : ISQLite
    {
        public SQLite.SQLiteConnection GetConnection()
        {
            var sqliteFilename = "PapafonAppSQLite.db3";

            string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData); // Documents folder

            var path = Path.Combine(documentsPath, sqliteFilename);

            System.Console.WriteLine(path);

            var conn = new SQLite.SQLiteConnection(path);

            return conn;
        }
    }
}