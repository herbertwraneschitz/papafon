﻿using System;
using System.IO;
using System.Threading.Tasks;
using Papafon.Droid;
using Papafon.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(SaveAndLoad_Android))]

namespace Papafon.Droid
{
    public class SaveAndLoad_Android : ISaveAndLoad
    {
        public string GetFullPath(string filename)
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), Path.GetFileName(filename));
        }

        public bool FileExists(string path)
        {
            return File.Exists(path);
        }

        public async Task<byte[]> LoadImageAsync(string filename)
        {
            try
            {
                var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), filename);

                var bytes = File.ReadAllBytes(path);

                return bytes;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<string> SaveImageAsync(string filename, byte[] image)
        {
            try
            {
                var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), filename);

                File.WriteAllBytes(path, image);

                return path;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}