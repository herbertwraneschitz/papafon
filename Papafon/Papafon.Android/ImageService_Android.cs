﻿using Android.Graphics;
using Papafon.Droid;
using Papafon.Interfaces;
using System;
using System.IO;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(ImageService_Android))]

namespace Papafon.Droid
{
    public class ImageService_Android : IImageService
    {
        public Task<byte[]> ResizeToExactSize(byte[] source, byte[] sourcePattern)
        {
            // First decode with inJustDecodeBounds=true to check dimensions
            var optionsGetSize = new BitmapFactory.Options()
            {
                InJustDecodeBounds = true,
                InPurgeable = true,
                InPreferredConfig = Bitmap.Config.Rgb565
            };

            var optionsDecode = new BitmapFactory.Options()
            {
                InJustDecodeBounds = false,
                InPurgeable = true,
                InPreferredConfig = Bitmap.Config.Rgb565
            };

            int width = 0;
            int height = 0;

            using (var imagePattern = BitmapFactory.DecodeByteArray(sourcePattern, 0, sourcePattern.Length, optionsGetSize))
            {
                width = optionsGetSize.OutWidth;
                height = optionsGetSize.OutHeight;
            }

            using (var image = BitmapFactory.DecodeByteArray(source, 0, source.Length, optionsDecode))
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    using (var bitmapScaled = Bitmap.CreateScaledBitmap(image, width, height, false))
                    {
                        bitmapScaled.Compress(Bitmap.CompressFormat.Jpeg, 100, ms);

                        bitmapScaled.Recycle();
                    }

                    image.Recycle();

                    return Task.FromResult<byte[]>(ms.ToArray());
                }
            }
        }

        public Task<byte[]> ResizeImage(byte[] source, float maxWidth, float maxHeight)
        {
            // First decode with inJustDecodeBounds=true to check dimensions
            var optionsGetSize = new BitmapFactory.Options()
            {
                InJustDecodeBounds = false,
                InPurgeable = true,
                InPreferredConfig = Bitmap.Config.Rgb565
            };

            using (var image = BitmapFactory.DecodeByteArray(source, 0, source.Length, optionsGetSize))
            {
                int width = (int)image.GetBitmapInfo().Width;
                int height = (int)image.GetBitmapInfo().Height;

                var maxResizeFactor = Math.Min(maxWidth / width, maxHeight / height);

                if (maxResizeFactor > 1)
                {
                    image.Recycle();

                    return Task.FromResult<byte[]>(source);
                }
                else
                {
                    var widthNew = (int)(maxResizeFactor * width);
                    var heightNew = (int)(maxResizeFactor * height);

                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (var bitmapScaled = Bitmap.CreateScaledBitmap(image, widthNew, heightNew, false))
                        {
                            bitmapScaled.Compress(Bitmap.CompressFormat.Jpeg, 100, ms);

                            bitmapScaled.Recycle();
                        }

                        image.Recycle();

                        return Task.FromResult<byte[]>(ms.ToArray());
                    }
                }
            }
        }
    }
}