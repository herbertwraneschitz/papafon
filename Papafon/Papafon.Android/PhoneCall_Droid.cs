﻿using System.Runtime.CompilerServices;
using Android.Content;
using Papafon.Droid;
using Xamarin.Forms;
using Papafon.Interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(PhoneCall_Droid))]
namespace Papafon.Droid
{
    public class PhoneCall_Droid : IPhoneCall
    {
        public void MakeQuickCall(string PhoneNumber)
        {
            try
            {
                var uri = Android.Net.Uri.Parse(string.Format("tel:{0}", PhoneNumber));
                var intent = new Intent(Intent.ActionCall, uri);
                Xamarin.Forms.Forms.Context.StartActivity(intent);
            }
            catch (System.Exception)
            {

                throw;
            }
        }

    }
}