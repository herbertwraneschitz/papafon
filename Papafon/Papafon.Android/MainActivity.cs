﻿
using Android.App;
using Android.Content.PM;
using Android.OS;
using HockeyApp.Android;
using ImageCircle.Forms.Plugin.Droid;
using Plugin.Permissions;
using System.Reflection;

namespace Papafon.Droid
{
    [Activity(Label = "SmartSenior", Icon = "@drawable/glasses", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        public static MainActivity instance;

        protected override void OnCreate(Bundle bundle)
        {
            instance = this;

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);

            CrashManager.Register(this, "f9c9f084a62a4b139594fef6d9c8f2e7");

            ImageCircleRenderer.Init();

            
            LoadApplication(new App());

            //var voiceIntent = new Intent(RecognizerIntent.ActionRecognizeSpeech);
            //voiceIntent.PutExtra(RecognizerIntent.ExtraLanguageModel, RecognizerIntent.LanguageModelFreeForm);

        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }




    }
}

